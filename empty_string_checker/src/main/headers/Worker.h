#ifndef UNTITLEDCLASSES_WORKER_H
#define UNTITLEDCLASSES_WORKER_H

#include <iostream>
#include <string>
#include <utility>

#include "Job.h"

class Worker {
private:
  long id = 0;

public:
  std::string name = "empty";
  Job job;
  std::string phone = "empty";
  int tier = 0;
  Worker(long id, std::string name, Job job, std::string phone,
         int tier);
  Worker();

  friend std::ostream &operator<<(std::ostream &os, const Worker &worker) {
    std::cout << worker.name;
  };

  const std::string &getName() const { return this->name; };
  void setName(const std::string &newname) { this->name = newname; };

  const Job &getJob() const { return this->job; };
  void setJob(const Job &newjob) { this->job = newjob; };

  const std::string &getPhone() const { return this->phone; };
  void setPhone(const std::string &newphone) { this->phone = newphone; };

  int getTier() const { return this->tier; };
  void setTier(int newtier) { this->tier = newtier; };
};

#endif // UNTITLEDCLASSES_WORKER_H