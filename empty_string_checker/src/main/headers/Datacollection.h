#ifndef UNTITLEDCLASSES_COLLECTION_H
#define UNTITLEDCLASSES_COLLECTION_H

#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "Contract.h"
#include "ProvidedService.h"
#include "Group.h"
#include "Worker.h"
#include "Job.h"

class Collection {
private:
  std::list<Contract> contracts;
  std::list<ProvidedService> services;
  std::list<Group> groups;
  std::list<Worker> workers;
  std::list<Job> jobs;

public:
  Collection(std::list<Contract> Contracts, std::list<Service> Services,
             std::list<Group> Groups, std::list<Worker> Workers,
             std::list<Job> Jobs);

  std::list<Contract> getActiveContracts();
};

#endif // UNTITLEDCLASSES_COLLECTION_H