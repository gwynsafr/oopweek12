#ifndef UNTITLEDCLASSES_JOB_H
#define UNTITLEDCLASSES_JOB_H

#include <iostream>
#include <string>
#include <utility>

class Job {
private:
  long id = 0;
  std::string name = "empty";
public:
  Job(long id, std::string name, double salary);
  Job();

  friend std::ostream &operator<<(std::ostream &os, const Job &job) {
    std::cout << job.name << " | " << std::endl;
  };

  const std::string &getName() const { return this->name; };
  void setName(const std::string &newname) { this->name = newname; };

  virtual double calculatePayment() = 0;
};

#endif // UNTITLEDCLASSES_JOB_H